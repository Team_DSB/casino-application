## Casino Application

The casino application is a **console JAVA application**, which main purpose is to register players, 
so that they can login and play casino games **(BlackJack & Roulette)**.

The application has one admin, which is previously chosen and has the abilities to:
	1. **View** all the information about the players.
	2. **Lock** a current player profile.
	3. **Unlock** a current player profile.
	4. **Start/stop** a game.
	5. **Check** game status.

The application has undefined number of players, which have the abilities to:
	1. **Register** at first, so that they can login afterwards.
	2. **View** their profile settings and **change** something.
	3. **Deposit** money.
	4. **Withdraw** money.
	5. Start **playing** games.