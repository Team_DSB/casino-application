package com.isoft.casino.container.api;

import java.io.File;
import java.util.Map;

import com.isoft.casino.container.impl.UserContainerImpl;
import com.isoft.casino.model.User;

/**
 * Interface class who serves for defining the methods that we need to save
 * users in a virtual database(container)
 * 
 * @author Deyan Georgiev
 * @see UserContainerImpl
 */
public interface UserContainer {

	/**
	 * Method for returning all the registered users
	 * 
	 * @return all registered users
	 */
	public Map<String, User> getUsers();

	/**
	 * Method which checks if user exists
	 * 
	 * @param nickname - the nickname of the user
	 * @return true or false if the user exists
	 */
	public boolean isUserExisting(String nicknameOrEmail);

	/**
	 * Method for adding a user to the container
	 * 
	 * @param user
	 */
	public void addUser(User user);

	/**
	 * Method for getting the users with locked profile
	 * 
	 * @return the users which have a locked profile
	 */
	public Map<String, User> getLockedUsers();

	/**
	 * Method for getting the users with unlocked profile
	 * 
	 * @return the users which have a unlocked profile
	 */
	public Map<String, User> getUnlockedUsers();

	/**
	 * Method for getting the current user by nickname
	 * 
	 * @param nickName of the user
	 * @return the user
	 */
	public User getUserByNickName(String nickName);

	/**
	 * Method for getting the current user by nickname
	 * 
	 * @param nickName of the user
	 * @return the user
	 */
	public User getUserByEmail(String email);

	/**
	 * Method for getting the logged-in user
	 */
	public User getLoggedInUser();

	/**
	 * Method for re-filling and re-updating the container with users
	 * 
	 * @param file the file from which to update the container
	 */
	public void updateContainer(File file);
}
