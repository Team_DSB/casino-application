package com.isoft.casino.container.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.container.api.UserContainer;
import com.isoft.casino.model.Admin;
import com.isoft.casino.model.Player;
import com.isoft.casino.model.User;

/**
 * User container implementation class which main purpose is based on adding
 * user, getting them, checking if a user exists and storing them as a virtual
 * database.
 * 
 * @author Deyan Georgiev
 * @see UserContainer
 */
public class UserContainerImpl implements UserContainer {
	private static final UserContainer CONTAINER_INSTANCE = new UserContainerImpl();
	private static final Logger LOGGER = Logger.getLogger(UserContainerImpl.class);
	private Map<String, User> usersWithNicknameKey = new HashMap<>();
	private Map<String, User> usersWithEmailKey = new HashMap<>();

	@Override
	public Map<String, User> getUsers() {
		return usersWithNicknameKey;
	}

	@Override
	public boolean isUserExisting(String nicknameOrEmail) {
		if (nicknameOrEmail.contains(GlobalConstants.EMAIL_IDENTIFIER)) {
			return usersWithEmailKey.containsKey(nicknameOrEmail);
		}
		return usersWithNicknameKey.containsKey(nicknameOrEmail);
	}

	@Override
	public void addUser(User user) {
		usersWithNicknameKey.put(user.getNickname(), user);
		usersWithEmailKey.put(user.getEmail(), user);
	}

	@Override
	public Map<String, User> getLockedUsers() {
		return usersWithNicknameKey.entrySet().stream().filter(map -> map.getValue().isLocked())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	@Override
	public Map<String, User> getUnlockedUsers() {
		return usersWithNicknameKey.entrySet().stream().filter(map -> !map.getValue().isLocked())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	@Override
	public User getUserByNickName(String nickName) {
		return usersWithNicknameKey.get(nickName);
	}

	@Override
	public User getUserByEmail(String email) {
		return usersWithEmailKey.get(email);
	}


	@Override
	public User getLoggedInUser() {
		Optional<Entry<String, User>> loggedInUserMap = usersWithNicknameKey.entrySet().stream()
				.filter(map -> map.getValue().isLoggedIn()).findFirst();
		if (loggedInUserMap.isPresent()) {
			return loggedInUserMap.get().getValue();
		}
		return null;
	}

	@Override
	public void updateContainer(File file) {
		this.usersWithEmailKey.clear();
		this.usersWithNicknameKey.clear();
		
		UserContainerImpl.getInstance().addUser(Admin.getInstance());
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		} catch (FileNotFoundException fnf) {
			LOGGER.error(fnf.getMessage());
		}

		Gson gson = new GsonBuilder().create();
		Player[] players = gson.fromJson(reader, Player[].class);
		for (int i = 0; i < players.length; i++) {
			UserContainerImpl.getInstance().addUser(players[i]);
		}
	}

	public static UserContainer getInstance() {
		return CONTAINER_INSTANCE;
	}
}
