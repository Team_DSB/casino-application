package com.isoft.casino;

import com.isoft.casino.menu.api.Menu;
import com.isoft.casino.menu.impl.LanguageMenu;
import com.isoft.casino.menu.impl.MainMenu;

public class Application {

	public static void main(String[] args){
		LanguageMenu langMenu = new LanguageMenu();
		langMenu.initializeMenu();
		Menu mainMenu = new MainMenu();
		mainMenu.initializeMenu();
	}

}
