package com.isoft.casino.exception;

/**
 * A custom exception whether the player's name is not in the appropriate
 * format.
 * 
 * @author Borislav Stoychev
 *
 */
public class InvalidNameFormatException extends Exception {

	private static final long serialVersionUID = -308380889300989254L;

	public InvalidNameFormatException(String message) {
		super(message);
	}
}
