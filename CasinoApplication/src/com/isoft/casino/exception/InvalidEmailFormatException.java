package com.isoft.casino.exception;

/**
 * A custom exception whether the email is in the appropriate format.
 * 
 * @author Borislav Stoychev
 *
 */
public class InvalidEmailFormatException extends Exception {

	private static final long serialVersionUID = 5567494690538320595L;

	public InvalidEmailFormatException(String message) {
		super(message);
	}
}
