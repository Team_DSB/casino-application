package com.isoft.casino.exception;

/**
 * A custom exception whether the nickname doesn't exist.
 * 
 * @author Borislav Stoychev
 *
 */
public class NotExistingNicknameException extends Exception {

	private static final long serialVersionUID = 8681358921338035739L;

	public NotExistingNicknameException(String message) {
		super(message);
	}
}
