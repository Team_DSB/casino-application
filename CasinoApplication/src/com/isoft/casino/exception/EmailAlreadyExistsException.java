package com.isoft.casino.exception;

/**
 * A custom exception whether the email already exists.
 * 
 * @author Borislav Stoychev
 *
 */
public class EmailAlreadyExistsException extends Exception {

	private static final long serialVersionUID = 6421800891747984507L;

	public EmailAlreadyExistsException(String message) {
		super(message);
	}
}
