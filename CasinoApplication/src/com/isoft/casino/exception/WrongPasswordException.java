package com.isoft.casino.exception;

/**
 * A custom exception for wrong password.
 * 
 * @author Borislav Stoychev
 *
 */
public class WrongPasswordException extends Exception {

	private static final long serialVersionUID = 5962923667368617595L;

	public WrongPasswordException(String message) {
		super(message);
	}
}
