package com.isoft.casino.exception;

/**
 * A custom exception whether the nickname already exists.
 * 
 * @author Borislav Stoychev
 *
 */
public class NicknameAlreadyExistsException extends Exception {

	private static final long serialVersionUID = 1444404591713098134L;

	public NicknameAlreadyExistsException(String message) {
		super(message);
	}
}
