package com.isoft.casino.exception;

/**
 * A custom exception whether the passwords don't match.
 * 
 * @author Borislav Stoychev
 *
 */
public class MismatchingPasswordsException extends Exception {

	private static final long serialVersionUID = 6556379170615293125L;

	public MismatchingPasswordsException(String message) {
		super(message);
	}
}
