package com.isoft.casino.exception;

/**
 * A custom exception whether the player is locked.
 * 
 * @author Borislav Stoychev
 *
 */
public class LockedPlayerException extends Exception {

	private static final long serialVersionUID = 3826565999518172280L;

	public LockedPlayerException(String message) {
		super(message);
	}
}
