package com.isoft.casino.exception;

/**
 * A custom exception whether the password is not in the appropriate format.
 * 
 * @author Borislav Stoychev
 *
 */
public class InvalidPasswordFormatException extends Exception {

	private static final long serialVersionUID = -5489848321857971335L;

	public InvalidPasswordFormatException(String message) {
		super(message);
	}
}
