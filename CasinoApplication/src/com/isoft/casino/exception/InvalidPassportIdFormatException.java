package com.isoft.casino.exception;

/**
 * A custom exception whether the passport ID is in the appropriate format.
 * 
 * @author Borislav Stoychev
 *
 */
public class InvalidPassportIdFormatException extends Exception {

	private static final long serialVersionUID = -1502831448925332344L;

	public InvalidPassportIdFormatException(String message) {
		super(message);
	}
}
