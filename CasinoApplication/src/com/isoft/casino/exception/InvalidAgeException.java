package com.isoft.casino.exception;

/**
 * A custom exception whether the player's age is valid.
 * 
 * @author Borislav Stoychev
 *
 */
public class InvalidAgeException extends Exception {

	private static final long serialVersionUID = 4172583121635508210L;

	public InvalidAgeException(String message) {
		super(message);
	}
}
