package com.isoft.casino.util.api;

import com.isoft.casino.exception.NicknameAlreadyExistsException;
import com.isoft.casino.exception.NotExistingNicknameException;

/**
 * Interface validator, which main purpose is to validate the nickname of the
 * user.
 * 
 * @author Borislav Stoychev
 *
 */
public interface NicknameValidator {

	/**
	 * 
	 * @param nickname
	 * @throws NicknameAlreadyExistsException
	 */
	public void isExistingNicknameOrEmail(String nicknameOrEmail) throws NicknameAlreadyExistsException;

	/**
	 * 
	 * @param nickname
	 * @throws NotExistingNicknameException
	 */
	public void isNotExistingNicknameOrEmail(String nicknameOrEmail) throws NotExistingNicknameException;
}
