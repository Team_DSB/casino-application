package com.isoft.casino.util.api;

import com.isoft.casino.exception.InvalidEmailFormatException;
import com.isoft.casino.util.impl.ValidatorUtilImpl;

/**
 * Interface validator, which main purpose is to valid the email of the user
 * 
 * @author Sonya Lazarova
 * @see ValidatorUtilImpl
 */
public interface EmailValidator {

	/**
	 * @param the email of the user
	 */
	public void validateEmail(String email) throws InvalidEmailFormatException;
}
