package com.isoft.casino.util.api;

import com.isoft.casino.exception.InvalidPasswordFormatException;
import com.isoft.casino.exception.MismatchingPasswordsException;
import com.isoft.casino.util.impl.ValidatorUtilImpl;

/**
 * Interface validator, which main purpose is to valid the password of the user.
 * 
 * @author Sonya Lazarova
 * @see ValidatorUtilImpl
 */
public interface PasswordValidator {

	/**
	 * @param the password of the user
	 * @throws InvalidPaswordFormatException
	 */
	public void validatePassword(String password) throws InvalidPasswordFormatException;

	/**
	 * 
	 * @param confirmPassword password which has to match with the user's password
	 * @param password        the user's password
	 * @throws MismatchingPasswordsException
	 */
	public void validateConfirmedPassword(String confirmPassword, String password) throws MismatchingPasswordsException;
}
