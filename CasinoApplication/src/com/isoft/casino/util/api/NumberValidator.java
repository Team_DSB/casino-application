package com.isoft.casino.util.api;

public interface NumberValidator {
	public double validateDoubleNumber(String number);
	public int validateIntegerNumber(String number);
}
