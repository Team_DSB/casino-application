package com.isoft.casino.util.api;

import com.isoft.casino.exception.InvalidPassportIdFormatException;

/**
 * Interface validator, which main purpose is to valid the passport id of the
 * user
 * 
 * @author Deyan Georgiev
 */
public interface PassportValidator {

	/**
	 * @param passport of the user
	 */
	public void validatePassport(String passport) throws InvalidPassportIdFormatException;
}
