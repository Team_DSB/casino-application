package com.isoft.casino.util.api;

import com.isoft.casino.exception.InvalidAgeException;
import com.isoft.casino.util.impl.ValidatorUtilImpl;

/**
 * Interface validator, which main purpose is to validate the age of the user
 * 
 * @author Deyan Georgiev
 * @see ValidatorUtilImpl
 */
public interface AgeValidator {

	/**
	 * @param age of the user
	 */
	public void validateAge(String age) throws InvalidAgeException;
}
