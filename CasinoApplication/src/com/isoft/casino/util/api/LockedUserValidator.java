package com.isoft.casino.util.api;

import com.isoft.casino.exception.LockedPlayerException;

/**
 * Interface validator, which main purpose is to valid whether the user is
 * locked.
 * 
 * @author Borislav Stoychev
 *
 */
public interface LockedUserValidator {

	/**
	 * 
	 * @param nickname
	 * @throws LockedPlayerException
	 */
	public void isLockedUser(String nickname) throws LockedPlayerException;
}
