package com.isoft.casino.util.api;

import com.isoft.casino.exception.InvalidNameFormatException;
import com.isoft.casino.util.impl.ValidatorUtilImpl;

/**
 * Interface validator, which main purpose is to valid all names of the user
 * 
 * @author Deyan Georgiev
 * @see ValidatorUtilImpl
 */
public interface NameValidator {

	/**
	 * @param name of the user
	 */
	void validateName(String name) throws InvalidNameFormatException;
}
