package com.isoft.casino.util.impl;

import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.container.impl.UserContainerImpl;
import com.isoft.casino.exception.InvalidAgeException;
import com.isoft.casino.exception.InvalidEmailFormatException;
import com.isoft.casino.exception.InvalidNameFormatException;
import com.isoft.casino.exception.InvalidPassportIdFormatException;
import com.isoft.casino.exception.InvalidPasswordFormatException;
import com.isoft.casino.exception.LockedPlayerException;
import com.isoft.casino.exception.MismatchingPasswordsException;
import com.isoft.casino.exception.NicknameAlreadyExistsException;
import com.isoft.casino.exception.NotExistingNicknameException;
import com.isoft.casino.language.Messages;
import com.isoft.casino.util.api.AgeValidator;
import com.isoft.casino.util.api.EmailValidator;
import com.isoft.casino.util.api.LockedUserValidator;
import com.isoft.casino.util.api.NameValidator;
import com.isoft.casino.util.api.NicknameValidator;
import com.isoft.casino.util.api.NumberValidator;
import com.isoft.casino.util.api.PassportValidator;
import com.isoft.casino.util.api.PasswordValidator;

/**
 * Implemenation of all the validators, needed for the application to valid the
 * user and the container
 * 
 * @author Deyan Georgiev
 * @see NameValidator
 * @see AgeValidator
 */

public class ValidatorUtilImpl implements NameValidator, AgeValidator, EmailValidator, PassportValidator,
		PasswordValidator, NicknameValidator, LockedUserValidator, NumberValidator {

	private Messages messages = new Messages();

	@Override
	public void validateName(String name) throws InvalidNameFormatException {
		if (name.length() < 3) {
			throw new InvalidNameFormatException(messages.getInvalidNameFormatExceptionMessage());
		}
	}

	@Override
	public void isExistingNicknameOrEmail(String nicknameOrEmail) throws NicknameAlreadyExistsException {
		if (UserContainerImpl.getInstance().isUserExisting(nicknameOrEmail)) {
			throw new NicknameAlreadyExistsException(messages.getNicknameAlreadyExistsExceptionMessage());
		}
	}

	@Override
	public void isNotExistingNicknameOrEmail(String nicknameOrEmail) throws NotExistingNicknameException {
		if (!UserContainerImpl.getInstance().isUserExisting(nicknameOrEmail)) {
			throw new NotExistingNicknameException(messages.getNotExistingNicknameExceptionMessage());
		}
	}

	@Override
	public void isLockedUser(String nickname) throws LockedPlayerException {
		if (UserContainerImpl.getInstance().getUserByNickName(nickname).isLocked()) {
			throw new LockedPlayerException(messages.getLockedPlayerExceptionMessage());
		}
	}

	@Override
	public void validateAge(String age) throws InvalidAgeException {
		if (Integer.parseInt(age) < 18) {
			throw new InvalidAgeException(messages.getInvalidAgeExceptionMessage());
		}
	}

	@Override
	public double validateDoubleNumber(String number) throws NumberFormatException {
		try {
			return Double.parseDouble(number);
		} catch (Exception e) {
			throw new NumberFormatException(messages.getInvalidNumberMessage());
		}
	}
	
	@Override
	public int validateIntegerNumber(String number) throws NumberFormatException {
		try {
			return Integer.parseInt(number);
		} catch (Exception e) {
			throw new NumberFormatException(messages.getInvalidNumberMessage());
		}
	}

	@Override
	public void validateEmail(String email) throws InvalidEmailFormatException {
		if (!email.matches(GlobalConstants.EMAIL_REGEX)) {
			throw new InvalidEmailFormatException(messages.getInvalidEmailFormatExceptionMessage());
		}
	}

	@Override
	public void validatePassport(String passport) throws InvalidPassportIdFormatException {
		if (!passport.matches(GlobalConstants.PASSPORT_REGEX)) {
			throw new InvalidPassportIdFormatException(messages.getInvalidPassportIdFormatExceptionMessage());
		}
	}

	@Override
	public void validatePassword(String password) throws InvalidPasswordFormatException {
		if (password.length() < 4) {
			throw new InvalidPasswordFormatException(messages.getInvalidPasswordFormatExceptionMessage());
		}
	}

	@Override
	public void validateConfirmedPassword(String confirmPassword, String password)
			throws MismatchingPasswordsException {
		if (!confirmPassword.equals(password)) {
			throw new MismatchingPasswordsException(messages.getMismatchingPasswordsExceptionMessage());
		}
	}
}
