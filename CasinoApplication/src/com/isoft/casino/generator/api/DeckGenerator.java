package com.isoft.casino.generator.api;

import java.util.List;

import com.isoft.casino.game.impl.blackjack.Card;

public interface DeckGenerator {
	public List<Card> generateDeck();
}
