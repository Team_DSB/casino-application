package com.isoft.casino.generator.api;

public interface BooleanGenerator {
	public boolean generateBoolean();
}
