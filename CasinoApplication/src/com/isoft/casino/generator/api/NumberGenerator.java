package com.isoft.casino.generator.api;

public interface NumberGenerator {
	public int generateNumber(int fromNumber, int toNumber);
}
