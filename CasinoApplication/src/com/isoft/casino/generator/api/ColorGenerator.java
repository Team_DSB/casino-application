package com.isoft.casino.generator.api;

public interface ColorGenerator {
	public String generateColor();
}
