package com.isoft.casino.generator.api;

import com.isoft.casino.game.impl.roulette.Division;

public interface DivisionGenerator {
	public Division generateDivision();
}
