package com.isoft.casino.generator.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.isoft.casino.game.impl.blackjack.Card;
import com.isoft.casino.game.impl.blackjack.CardColor;
import com.isoft.casino.game.impl.blackjack.CardName;
import com.isoft.casino.game.impl.blackjack.Deck;
import com.isoft.casino.generator.api.DeckGenerator;
import com.isoft.casino.generator.api.NumberGenerator;

public class BlackjackGenerator implements NumberGenerator, DeckGenerator {
	private Random random = new Random();

	@Override
	public int generateNumber(int fromNumber, int toNumber) {
		return random.nextInt((toNumber - fromNumber) + 1) + fromNumber;
	}

	@Override
	public List<Card> generateDeck() {
		Deck deck = new Deck(new ArrayList<Card>());

		for (CardName name : CardName.values()) {
			for (CardColor color : CardColor.values()) {
				Card card = new Card(name, color);

				deck.addCard(card);
			}
		}

		return deck.getDeck();
	}
}
