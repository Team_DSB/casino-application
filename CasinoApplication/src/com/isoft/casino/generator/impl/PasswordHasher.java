package com.isoft.casino.generator.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

import com.isoft.casino.constants.GlobalConstants;

/**
 * A class for hashing the user's password.
 * @author Borislav Stoychev
 *
 */
public class PasswordHasher {
	private static final Logger LOGGER = Logger.getLogger(PasswordHasher.class);
	
	/**
	 * private constructor to prevent instantiation
	 */
	private PasswordHasher() {}

	public static String hashPassword(String passwordToHash) {
		StringBuilder sb = new StringBuilder();

		try {
			MessageDigest md = MessageDigest.getInstance(GlobalConstants.MD5_HASH_NAME);
			md.update(passwordToHash.getBytes());
			byte[] bytes = md.digest();

			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error(e);
		}

		return sb.toString();
	}
}
