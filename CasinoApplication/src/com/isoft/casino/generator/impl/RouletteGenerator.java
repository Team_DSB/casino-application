package com.isoft.casino.generator.impl;

import java.util.List;
import java.util.Random;

import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.game.impl.roulette.Division;
import com.isoft.casino.generator.api.DivisionGenerator;
import com.isoft.casino.generator.api.NumberGenerator;

public class RouletteGenerator implements NumberGenerator, DivisionGenerator {
	private List<Division> divisions;
	private Random random = new Random();

	/**
	 * @param divisions
	 */
	public RouletteGenerator(List<Division> divisions) {
		this.divisions = divisions;
	}

	@Override
	public Division generateDivision() {
		return divisions.get(generateNumber(GlobalConstants.DIVISION_START_NUMBER_GENERATOR,
				GlobalConstants.DIVISION_END_NUMBER_GENERATOR));
	}

	@Override
	public int generateNumber(int fromNumber, int toNumber) {
		return random.nextInt((toNumber - fromNumber) + 1) + fromNumber;
	}
}
