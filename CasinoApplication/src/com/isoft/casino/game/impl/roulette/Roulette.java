package com.isoft.casino.game.impl.roulette;

import java.util.List;

import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.game.impl.Game;
import com.isoft.casino.generator.impl.RouletteGenerator;

public class Roulette extends Game {
	private List<Division> divisions;
	private Division betDivision;
	private Division winningDivision;
	private int coefficent;

	/**
	 * @param divisions
	 * @param coefficent
	 */
	public Roulette(List<Division> divisions) {
		this.divisions = divisions;
		this.betDivision = new Division();
		this.winningDivision = new Division();
		this.coefficent = GlobalConstants.ROULLETTE_ZERO_COEFFICIENT;
	}

	public void playByColor(String color) {
		coefficent = GlobalConstants.ROULLETTE_TWO_COEFFICIENT;
		betDivision.setColor(color);
	}

	public void playByNumber(int number) {
		coefficent = GlobalConstants.ROULLETTE_THIRTY_SIX_COEFFICIENT;
		betDivision.setNumber(number);
	}

	public void playBySector(int sector) {
		coefficent = GlobalConstants.ROULLETTE_THREE_COEFFICIENT;
		betDivision.setSector(sector);
	}

	public void spin() {
		winningDivision = new RouletteGenerator(divisions).generateDivision();
	}

	public boolean isWinning() {
		if (coefficent == GlobalConstants.ROULLETTE_TWO_COEFFICIENT) {
			return betDivision.getColor().equals(winningDivision.getColor());
		}
		if (coefficent == GlobalConstants.ROULLETTE_THIRTY_SIX_COEFFICIENT) {
			return betDivision.getNumber() == winningDivision.getNumber();
		}
		if (coefficent == GlobalConstants.ROULLETTE_THREE_COEFFICIENT) {
			return betDivision.getSector() == winningDivision.getSector();
		}

		return false;
	}

	public int getCoefficent() {
		return coefficent;
	}
}
