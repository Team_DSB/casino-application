package com.isoft.casino.game.impl.roulette;

public class Division {
	private int number;
	private String color;
	private int sector;

	/**
	 * @param number
	 * @param color
	 * @param sector
	 */
	protected Division(int number, String color, int sector) {
		this.number = number;
		this.color = color;
		this.sector = sector;
	}

	/**
	 * 
	 */
	protected Division() {
	}

	public int getNumber() {
		return number;
	}

	public String getColor() {
		return color;
	}

	public int getSector() {
		return sector;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setSector(int sector) {
		this.sector = sector;
	}

	@Override
	public String toString() {
		return " " + number + ", " + color + ", sector" + sector;
	}

}
