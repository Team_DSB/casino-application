package com.isoft.casino.game.impl;

import com.isoft.casino.game.api.AdminGame;

public abstract class AdminGameManager implements AdminGame {
	Game game;

	/**
	 * @param player
	 * @param game
	 */
	protected AdminGameManager(Game game) {
		this.game = game;
	}

	@Override
	public void enable() {
		game.changeStatus(true);
	}

	@Override
	public void disable() {
		game.changeStatus(false);
	}

	@Override
	public boolean checkStatus() {
		return game.isStatus();
	}
}
