package com.isoft.casino.game.impl;

public abstract class Game {
	private double bet;
	private boolean status;

	/**
	 * @param bet
	 * @param status
	 */
	protected Game() {
		this.bet = 0;
		this.status = false;
	}

	public double getBet() {
		return bet;
	}

	public void changeBet(double bet) {
		this.bet = bet;
	}

	public boolean isStatus() {
		return status;
	}

	public void changeStatus(boolean status) {
		this.status = status;
	}
}
