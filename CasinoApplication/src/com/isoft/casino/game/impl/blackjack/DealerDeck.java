package com.isoft.casino.game.impl.blackjack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.isoft.casino.generator.impl.BlackjackGenerator;

public class DealerDeck {
	private List<Card> deck;
	private List<Card> usedCards;

	/**
	 * 
	 */
	protected DealerDeck() {
		for (int i = 0; i < 8; i++) {
			deck.addAll(new BlackjackGenerator().generateDeck());
		}

		Collections.shuffle(deck);

		this.usedCards = new ArrayList<>();
	}

	public Card hitCard() {
		Card card = deck.get(deck.size());

		deck.remove(deck.size());
		usedCards.add(card);

		return card;
	}
}
