package com.isoft.casino.game.impl.blackjack;

import java.util.List;

public class Deck {
	List<Card> cards;

	/**
	 * @param cards
	 */
	public Deck(List<Card> cards) {
		this.cards = cards;
	}

	public void addCard(Card card) {
		cards.add(card);
	}

	public List<Card> getDeck() {
		return cards;
	}
}
