package com.isoft.casino.game.impl.blackjack;

import java.util.ArrayList;

import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.game.impl.Game;

public class Blackjack extends Game {
	private int coefficent;
	private Hand playerHand;
	private boolean isStand;
	private Dealer dealer;

	/**
	 * @param coefficent
	 * @param hand
	 * @param isStand
	 */
	protected Blackjack(int coefficent) {
		this.coefficent = coefficent;
		this.playerHand = new Hand(new ArrayList<Card>());
		this.isStand = false;
	}

	public void hit() {
		playerHand.addCard(dealer.hitCard());
	}

	public void doubleDown() {
		changeBet(2 * getBet());
		hit();
		stand();
	}

	public void stand() {
		isStand = true;
	}

	public boolean isStand() {
		return isStand;
	}

	public void hitCardToDealer() {
		while (dealer.getHand().getValue() <= playerHand.getValue()) {
			dealer.addCard(dealer.hitCard());
			if (dealer.getHand().getValue() > GlobalConstants.BLACKJACK_MAX_VALUE) {
				if (dealer.getHand().hasAce()) {
					dealer.getHand().changeAceValue();
				}
			} else {
				break;
			}
		}
	}

	public boolean isWinning() {
		return playerHand.getValue() == GlobalConstants.BLACKJACK_MAX_VALUE 
				|| dealer.getHand().getValue() > GlobalConstants.BLACKJACK_MAX_VALUE;
	}
}
