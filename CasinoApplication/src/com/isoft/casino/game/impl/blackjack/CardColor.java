package com.isoft.casino.game.impl.blackjack;

public enum CardColor {
	SPADES, HEARTS, DIAMONDS, CLUBS
}
