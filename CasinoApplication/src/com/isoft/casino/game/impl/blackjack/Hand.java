package com.isoft.casino.game.impl.blackjack;

import java.util.List;

import com.isoft.casino.constants.GlobalConstants;

public class Hand {
	private List<Card> cards;
	private int value;
	private Card lastAce;
	private boolean isValueCalculated;

	/**
	 * @param cards
	 * @param value
	 */
	protected Hand(List<Card> cards) {
		this.cards = cards;
		this.isValueCalculated = false;
	}

	public Card getCard(int indexOfCard) {
		return cards.get(indexOfCard);
	}

	public int getValue() {
		if (!isValueCalculated) {
			value = calculateCardValue();
		}

		return value;
	}

	public boolean hasAce() {
		return lastAce != null;
	}

	public void addCard(Card card) {
		if (card.getName() == CardName.ACE) {
			lastAce = card;
		}
		cards.add(card);
		isValueCalculated = false;
	}

	public int calculateCardValue() {
		int cardValue = GlobalConstants.CARD_DEFAULT_VALUE;

		for (Card card : cards) {
			cardValue += card.getValue();
		}

		return cardValue;
	}

	public void changeAceValue() {
		cards.get(cards.lastIndexOf(lastAce)).changeValue(1);
		lastAce = null;
	}
}
