package com.isoft.casino.game.impl.blackjack;

import com.isoft.casino.constants.GlobalConstants;

public class Card {
	private CardName name;
	private CardColor color;
	private int value;

	/**
	 * @param name
	 * @param color
	 */
	public Card(CardName name, CardColor color) {
		this.name = name;
		this.color = color;
		this.value = calculateValue(name);
	}

	public int getValue() {
		return this.value;
	}

	public CardName getName() {
		return this.name;
	}

	private int calculateValue(CardName name) {
		int cardValue = GlobalConstants.CARD_DEFAULT_VALUE;

		switch (name) {
		case TWO:
			cardValue = GlobalConstants.CARD_TWO_VALUE;
			break;
		case THREE:
			cardValue = GlobalConstants.CARD_THREE_VALUE;
			break;
		case FOUR:
			cardValue = GlobalConstants.CARD_FOUR_VALUE;
			break;
		case FIVE:
			cardValue = GlobalConstants.CARD_FIVE_VALUE;
			break;
		case SIX:
			cardValue = GlobalConstants.CARD_SIX_VALUE;
			break;
		case SEVEN:
			cardValue = GlobalConstants.CARD_SEVEN_VALUE;
			break;
		case EIGHT:
			cardValue = GlobalConstants.CARD_EIGHT_VALUE;
			break;
		case NINE:
			cardValue = GlobalConstants.CARD_NINE_VALUE;
			break;
		case TEN:
		case JACK:
		case KING:
		case QUEEN:
		case ACE:
			cardValue = GlobalConstants.CARD_TEN_VALUE;
			break;
		}

		return cardValue;
	}

	public void changeValue(int value) {
		this.value = value;
	}
}
