package com.isoft.casino.game.impl.blackjack;

public class Dealer {
	private Hand hand;
	private DealerDeck deck;

	/**
	 * 
	 */
	protected Dealer(Hand hand) {
		this.hand = hand;
		this.deck = new DealerDeck();
	}

	public Hand getHand() {
		return this.hand;
	}

	public void addCard(Card card) {
		hand.addCard(card);
	}

	public Card hitCard() {
		return deck.hitCard();
	}
}
