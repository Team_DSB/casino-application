package com.isoft.casino.game.impl;

import com.isoft.casino.game.api.PlayerGame;
import com.isoft.casino.model.Player;

public class PlayerGameManager implements PlayerGame {
	Player player;
	Game game;

	/**
	 * @param player
	 * @param game
	 */
	public PlayerGameManager(Player player, Game game) {
		this.player = player;
		this.game = game;
	}

	@Override
	public void placeBet(double bet) {
		game.changeBet(bet);
	}

	@Override
	public Game getGame() {
		return game;
	}

	@Override
	public Player getPlayer() {
		return this.player;
	}
}
