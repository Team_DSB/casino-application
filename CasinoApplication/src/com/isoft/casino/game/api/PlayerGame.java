package com.isoft.casino.game.api;

import com.isoft.casino.game.impl.Game;
import com.isoft.casino.model.Player;

public interface PlayerGame {
	public Game getGame();
	
	public Player getPlayer();

	public void placeBet(double bet);
}
