package com.isoft.casino.game.api;

public interface AdminGame {
	public void enable();

	public void disable();

	public boolean checkStatus();
}
