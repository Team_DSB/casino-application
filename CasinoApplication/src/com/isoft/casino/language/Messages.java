package com.isoft.casino.language;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.io.api.FileReader;
import com.isoft.casino.io.impl.FileReaderImpl;

/**
 * A class for getting the messages of the current language.
 * @author Borislav Stoychev
 *
 */
public class Messages {

	private static final Logger LOGGER = Logger.getLogger(Messages.class);
	
	private static final String APPLICATION_DIRECTORY = System.getProperty(GlobalConstants.USER_DIRECTORY_PROPERTY);
	private static final String MESSAGES_FILE_JSON = GlobalConstants.MESSAGES_FILE_PATH;
	
	private final FileReader fileReader = new FileReaderImpl();
	private Map<String,String>currentLanguageMessages; 
	
	/**
	 * The constructor gets the content with the chosen language.
	 */
	public Messages()
	{
		File languageFile = readFile(APPLICATION_DIRECTORY+MESSAGES_FILE_JSON);
		Map<String, Map<String, String>> langContent = fileReader.convertFileToMap(languageFile);
		currentLanguageMessages = langContent.get(Language.getInstance().getLanguage());
	}
	
	public String getInvalidNameFormatExceptionMessage()
	{
		return currentLanguageMessages.get("invalidNameFormat");
	}
	
	public String getNicknameAlreadyExistsExceptionMessage()
	{
		return currentLanguageMessages.get("nicknameAlreadyExists");
	}
	
	public String getNotExistingNicknameExceptionMessage()
	{
		return currentLanguageMessages.get("notExistingNickname");
	}
	
	public String getLockedPlayerExceptionMessage()
	{
		return currentLanguageMessages.get("lockedPlayer");
	}
	
	public String getInvalidAgeExceptionMessage()
	{
		return currentLanguageMessages.get("invalidAge");
	}
	
	public String getInvalidAgeFormatExceptionMessage()
	{
		return currentLanguageMessages.get("invalidAgeFormat");
	}
	
	public String getInvalidEmailFormatExceptionMessage()
	{
		return currentLanguageMessages.get("invalidEmailFormat");
	}
	
	public String getInvalidPassportIdFormatExceptionMessage()
	{
		return currentLanguageMessages.get("invalidPassportIdFormat");
	}
	
	public String getInvalidPasswordFormatExceptionMessage()
	{
		return currentLanguageMessages.get("invalidPasswordFormat");
	}
	
	public String getMismatchingPasswordsExceptionMessage()
	{
		return currentLanguageMessages.get("mismatchingPasswords");
	}
	
	public String getWrongPasswordExceptionMessage()
	{
		return currentLanguageMessages.get("wrongPassword");
	}
	
	public String getAttemptsLeftMessage()
	{
		return currentLanguageMessages.get("attemptsLeft");
	}
	
	public String getInvalidChoiceMessage()
	{
		return currentLanguageMessages.get("invalidChoice");
	}
	
	public String getLogOutConfirmationMessage()
	{
		return currentLanguageMessages.get("logOutConfirmation");
	}
	
	public String getInvalidNumberMessage()
	{
		return currentLanguageMessages.get("invalidNumber");
	}
	
	public String getCannotWithdrawMoneyMessage()
	{
		return currentLanguageMessages.get("cannotWithdrawMoney");
	}
	
	public String getCannotDepositMoneyMessage()
	{
		return currentLanguageMessages.get("cannotDepositMoney");
	}
	
	public String getTypeWithdrawAmountMessage()
	{
		return currentLanguageMessages.get("typeWithdrawAmount");
	}
	
	public String getTypeDepositAmountMessage()
	{
		return currentLanguageMessages.get("typeDepositAmount");
	}
	
	public String getEnterNewFirstNameMessage()
	{
		return currentLanguageMessages.get("enterNewFirstName");
	}
	
	public String getEnterNewMiddleNameMessage()
	{
		return currentLanguageMessages.get("enterNewMiddleName");
	}
	
	public String getEnterNewLastNameMessage()
	{
		return currentLanguageMessages.get("enterNewLastName");
	}
	
	public String getEnterNewNickNameMessage()
	{
		return currentLanguageMessages.get("enterNewNickName");
	}
	
	public String getEnterNewAgeMessage()
	{
		return currentLanguageMessages.get("enterNewAge");
	}
	
	public String getEnterNewEmailAddressMessage()
	{
		return currentLanguageMessages.get("enterNewEmailAddress");
	}
	
	public String getEnterNewPassportIdMessage()
	{
		return currentLanguageMessages.get("enterNewPassportId");
	}
	
	public String getEnterCurrentPasswordMessage()
	{
		return currentLanguageMessages.get("enterCurrentPassword");
	}
	
	public String getEnterNewPasswordMessage()
	{
		return currentLanguageMessages.get("enterNewPassword");
	}
	
	public String getConfirmPasswordMessage()
	{
		return currentLanguageMessages.get("confirmPassword");
	}
	
	public String getCurrentFirstNameMessage()
	{
		return currentLanguageMessages.get("currentFirstName");
	}
	
	public String getCurrentMiddleNameMessage()
	{
		return currentLanguageMessages.get("currentMiddleName");
	}
	
	public String getCurrentLastNameMessage()
	{
		return currentLanguageMessages.get("currentLastName");
	}
	
	public String getCurrentNickNameMessage()
	{
		return currentLanguageMessages.get("currentNickName");
	}
	
	public String getCurrentEmailAddressMessage()
	{
		return currentLanguageMessages.get("currentEmailAddress");
	}
	
	public String getCurrentAgeMessage()
	{
		return currentLanguageMessages.get("currentAge");
	}
	
	public String getCurrentPassportIdMessage()
	{
		return currentLanguageMessages.get("currentPassportId");
	}
	
	public String getUnlockedPlayerMessage()
	{
		return currentLanguageMessages.get("unlockedPlayer");
	}
	
	public String getSuccessfulLoginMessage()
	{
		return currentLanguageMessages.get("successfulLogin");
	}
	
	public String getSuccessfulRegisterMessage()
	{
		return currentLanguageMessages.get("successfulRegister");
	}
	
	public String getCannotLockUserMessages()
	{
		return currentLanguageMessages.get("cannotLockUser");
	}
	
	public String getCannotUnlockUserMessages()
	{
		return currentLanguageMessages.get("cannotUnlockUser");
	}
	
	public String getYourMoneyMessage()
	{
		return currentLanguageMessages.get("yourMoney");
	}
	
	public String getPlaceBetMessage()
	{
		return currentLanguageMessages.get("placeBet");
	}
	
	public String getYouHaveLostMessage()
	{
		return currentLanguageMessages.get("youHaveLost");
	}
	
	public String getYouHaveWonMessage()
	{
		return currentLanguageMessages.get("youHaveWon");
	}
	
	public String getDoYouWantToContinueMessage()
	{
		return currentLanguageMessages.get("doYouWantToContinue");
	}
	
	public String getWinningDevisionMessage()
	{
		return currentLanguageMessages.get("winningDevision");
	}
	
	public String getYourCardsMessage()
	{
		return currentLanguageMessages.get("yourCards");
	}
	
	public String getDealerCards()
	{
		return currentLanguageMessages.get("dealerCards");
	}
	
	public String getBlackjackMessage()
	{
		return currentLanguageMessages.get("blackjack");
	}
	
	private File readFile(String path) 
	{
		File file = null;
		try 
		{
			file = fileReader.readFile(path);
		} 
		catch (IOException ioe) 
		{
			LOGGER.error(ioe);
		}

		return file;
	}
}
