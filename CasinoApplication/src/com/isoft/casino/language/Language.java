package com.isoft.casino.language;

/**
 * A singleton class for setting and getting the current language.
 * @author Borislav Stoychev
 *
 */
public class Language {

	private static final Language LANGUAGE_INSTANCE = new Language();
	private String language;
	
	public static Language getInstance() {
		return LANGUAGE_INSTANCE;
	}
	
	public void setLanguage(String language)
	{
		this.language = language;
	}
	
	public String getLanguage()
	{
		return this.language;
	}
}
