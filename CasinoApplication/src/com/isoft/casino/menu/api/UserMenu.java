package com.isoft.casino.menu.api;

import com.isoft.casino.menu.impl.MainMenu;
import com.isoft.casino.model.User;

/**
 * UserMenu interface serves for the login method of the main menu.
 * @see MainMenu
 * @author Borislav Stoychev
 *
 */
public interface UserMenu {

	/**
	 * Implements the login logic based on the user input as credentials
	 * (nickname, password)
	 * @return user the type of user (Admin | Player)
	 */
	public User login();
}
