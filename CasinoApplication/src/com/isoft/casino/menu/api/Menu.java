package com.isoft.casino.menu.api;

import com.isoft.casino.menu.impl.AdminMenu;
import com.isoft.casino.menu.impl.LanguageMenu;
import com.isoft.casino.menu.impl.MainMenu;
import com.isoft.casino.menu.impl.PlayerMenu;
import com.isoft.casino.menu.impl.PlayerProfileSettingsMenu;

/**
 * Menu interface serves for initializing the three menus:
 * @see AdminMenu
 * @see MainMenu
 * @see PlayerMenu
 * @see PlayerProfileSettingsMenu
 * @see LanguageMenu
 * @author Borislav Stoychev
 *
 */
public interface Menu {

	/**
	 * Initializing the given menu
	 */
	public void initializeMenu();
}
