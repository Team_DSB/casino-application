package com.isoft.casino.menu.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.container.impl.UserContainerImpl;
import com.isoft.casino.exception.InvalidAgeException;
import com.isoft.casino.exception.InvalidEmailFormatException;
import com.isoft.casino.exception.InvalidNameFormatException;
import com.isoft.casino.exception.InvalidPassportIdFormatException;
import com.isoft.casino.exception.InvalidPasswordFormatException;
import com.isoft.casino.exception.MismatchingPasswordsException;
import com.isoft.casino.exception.WrongPasswordException;
import com.isoft.casino.generator.impl.PasswordHasher;
import com.isoft.casino.io.api.FileReader;
import com.isoft.casino.io.api.InputReader;
import com.isoft.casino.io.api.JsonFileWriter;
import com.isoft.casino.io.impl.ConsoleInputReader;
import com.isoft.casino.io.impl.FileReaderImpl;
import com.isoft.casino.io.impl.JsonFileWriterImpl;
import com.isoft.casino.language.Language;
import com.isoft.casino.language.Messages;
import com.isoft.casino.menu.api.Menu;
import com.isoft.casino.model.Player;
import com.isoft.casino.util.impl.ValidatorUtilImpl;

/**
 * Implementation of menu interface.
 * @see Menu
 * @author Borislav Stoychev
 *
 */
public class PlayerProfileSettingsMenu implements Menu{
	private static final String APPLICATION_DIRECTORY = System.getProperty(GlobalConstants.USER_DIRECTORY_PROPERTY);
	private static final String PLAYER_PROFILE_SETTINGS_MENU_FILE_JSON = GlobalConstants.PLAYER_PROFILE_SETTINGS_MENU_FILE_PATH;
	private static final String REGISTERED_PLAYERS_FILE_JSON = GlobalConstants.REGISTERED_PLAYERS_FILE_PATH;
	private static final Logger LOGGER = Logger.getLogger(PlayerProfileSettingsMenu.class);

	private final InputReader consoleReader = new ConsoleInputReader();
	private final FileReader fileReader = new FileReaderImpl();
	private final JsonFileWriter fileWriter = new JsonFileWriterImpl();
	private final ValidatorUtilImpl validator = new ValidatorUtilImpl();
	
	private Messages messages = new Messages();
	private Map<String, String>currentLangPlayerProfileSettingsMenu;
	
	@Override
	public void initializeMenu()
	{	
		File playerProfileSettingsMenuFile = readFile(APPLICATION_DIRECTORY + PLAYER_PROFILE_SETTINGS_MENU_FILE_JSON);

		Map<String, Map<String, String>>playerProfileSettingsMenuContent = fileReader.convertFileToMap(playerProfileSettingsMenuFile);
		
		currentLangPlayerProfileSettingsMenu = playerProfileSettingsMenuContent.get(Language.getInstance().getLanguage());
		profileSettingsMenuOptions();
	}
	
	private File readFile(String path) 
	{
		File file = null;
		try {
			file = fileReader.readFile(path);
		} catch (FileNotFoundException fnfe) {
			LOGGER.error(fnfe);
		} catch (IOException ioe) {
			LOGGER.error(ioe);
		}

		return file;
	}
	
	private String userInput(InputReader consInputReader)
	{
		String input=null;
		try 
		{
			input = consInputReader.readLine();
		} 
		catch (IOException e) 
		{
			LOGGER.error(e);
		}
		return input;
	}
	
	private void printCurrentMenu(Map<String, String> content) 
	{
		for (String str : content.values()) 
		{
			System.out.println(str);
		}
	}
	
	private void changeFirstName(Player loggedInPlayer) throws InvalidNameFormatException
	{
		System.out.println(messages.getEnterNewFirstNameMessage());
		String newFirstName = userInput(consoleReader);
		validator.validateName(newFirstName);
		loggedInPlayer.setFirstName(newFirstName);
	}
	
	private void changeMiddleName(Player loggedInPlayer) throws InvalidNameFormatException
	{
		System.out.println(messages.getEnterNewMiddleNameMessage());
		String newMiddleName = userInput(consoleReader);
		validator.validateName(newMiddleName);
		loggedInPlayer.setMiddleName(newMiddleName);
	}
	
	private void changeLastName(Player loggedInPlayer) throws InvalidNameFormatException
	{
		System.out.println(messages.getEnterNewLastNameMessage());
		String newLastName = userInput(consoleReader);
		validator.validateName(newLastName);
		loggedInPlayer.setLastName(newLastName);
	}
	
	private void changeNickName(Player loggedInPlayer) throws InvalidNameFormatException
	{
		System.out.println(messages.getEnterNewNickNameMessage());
		String newNickName = userInput(consoleReader);
		validator.validateName(newNickName);
		loggedInPlayer.setNickname(newNickName);
	}
	
	private void changeAge(Player loggedInPlayer) throws InvalidAgeException
	{
		System.out.println(messages.getEnterNewAgeMessage());
		String newAge = userInput(consoleReader);
		validator.validateAge(newAge);
		loggedInPlayer.setAge(Integer.parseInt(newAge));
	}
	
	private void changeEmail(Player loggedInPlayer) throws InvalidEmailFormatException
	{
		System.out.println(messages.getEnterNewEmailAddressMessage());
		String newEmail= userInput(consoleReader);
		validator.validateEmail(newEmail);
		loggedInPlayer.setEmail(newEmail);
	}
	
	private void changePassportId(Player loggedInPlayer) throws InvalidPassportIdFormatException
	{
		System.out.println(messages.getEnterNewPassportIdMessage());
		String newPassportId= userInput(consoleReader);
		validator.validatePassport(newPassportId);
		loggedInPlayer.setEmail(newPassportId);
	}
	
	private void changePassword(Player loggedInPlayer) throws InvalidPasswordFormatException, WrongPasswordException, MismatchingPasswordsException
	{
		System.out.println(messages.getEnterCurrentPasswordMessage());
		String currentPassword = PasswordHasher.hashPassword(userInput(consoleReader));
		if(!currentPassword.equals(loggedInPlayer.getPassword()))
		{
			throw new WrongPasswordException(messages.getWrongPasswordExceptionMessage());
		}
		else
		{
			System.out.println(messages.getEnterNewPasswordMessage());
			String newPassword = userInput(consoleReader);
			validator.validatePassword(newPassword);
			System.out.println(messages.getConfirmPasswordMessage());
			String confirmPassword = userInput(consoleReader);
			validator.validateConfirmedPassword(confirmPassword, newPassword);
			newPassword = PasswordHasher.hashPassword(newPassword);
			loggedInPlayer.setPassword(newPassword);
		}
	}
	
	private void profileSettingsMenuOptions()
	{
		Player loggedInPlayer = (Player)UserContainerImpl.getInstance().getLoggedInUser();
		String choice = null;
		boolean back = false;
		while(!back)
		{
			printCurrentMenu(currentLangPlayerProfileSettingsMenu);
			choice = userInput(consoleReader);
			try
			{
			   switch(choice)
			   {
			   case "1":
				   System.out.println(messages.getCurrentFirstNameMessage()+loggedInPlayer.getFirstName());
				   changeFirstName(loggedInPlayer);
				   break;
			   case "2":
				   System.out.println(messages.getCurrentMiddleNameMessage()+loggedInPlayer.getMiddleName());
				   changeMiddleName(loggedInPlayer);
				   break;
			   case "3":
				   System.out.println(messages.getCurrentLastNameMessage()+loggedInPlayer.getLastName());
				   changeLastName(loggedInPlayer);
				   break;
			   case "4":
				   System.out.println(messages.getCurrentAgeMessage()+loggedInPlayer.getAge());
				   changeAge(loggedInPlayer);
				   break;
			   case "5":
				   System.out.println(messages.getCurrentNickNameMessage()+loggedInPlayer.getNickname());
				   changeNickName(loggedInPlayer);
				   break;
			   case "6":
				   System.out.println(messages.getCurrentEmailAddressMessage()+loggedInPlayer.getEmail());
				   changeEmail(loggedInPlayer);
				   break;
			   case "7":
				   System.out.println(messages.getCurrentPassportIdMessage()+loggedInPlayer.getPassportId());
				   changePassportId(loggedInPlayer);
				   break;
			   case "8":
				   changePassword(loggedInPlayer);
				   break;
			   case "9": 
				   back = true;
				   fileWriter.writePlayersToJsonFile(new File(APPLICATION_DIRECTORY + REGISTERED_PLAYERS_FILE_JSON), 
						   loggedInPlayer, new ObjectMapper());
			       break;
			   default:
				   LOGGER.warn(messages.getInvalidChoiceMessage());
			   }
			}
			catch (InvalidNameFormatException 
					| NumberFormatException
					| InvalidAgeException 
					| InvalidEmailFormatException 
					| InvalidPassportIdFormatException 
					| InvalidPasswordFormatException
					| WrongPasswordException
					| MismatchingPasswordsException
					| IOException e)
			{
				LOGGER.error(e.getMessage());			
			}
		}
	}
	
}


