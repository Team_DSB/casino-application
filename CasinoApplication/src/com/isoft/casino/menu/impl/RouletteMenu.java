package com.isoft.casino.menu.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerRepository;

import com.isoft.casino.container.impl.UserContainerImpl;
import com.isoft.casino.game.impl.PlayerGameManager;
import com.isoft.casino.game.impl.roulette.Division;
import com.isoft.casino.game.impl.roulette.Roulette;
import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.io.api.FileReader;
import com.isoft.casino.io.api.InputReader;
import com.isoft.casino.io.impl.ConsoleInputReader;
import com.isoft.casino.io.impl.FileReaderImpl;
import com.isoft.casino.language.Language;
import com.isoft.casino.language.Messages;
import com.isoft.casino.menu.api.Menu;
import com.isoft.casino.model.Player;
import com.isoft.casino.util.impl.ValidatorUtilImpl;

public class RouletteMenu implements Menu {

	private final InputReader consoleReader = new ConsoleInputReader();
	private final FileReader fileReader = new FileReaderImpl();
	private static final Logger LOGGER = Logger.getLogger(RouletteMenu.class);

	private Messages messages = new Messages();
	private Map<String, String> currentLangRouletteMenu;
	private static final String APPLICATION_DIRECTORY = System.getProperty("user.dir");
	private static final String ROULETTE_MENU_FILE_JSON = "/src/com/isoft/casino/resource/file/roulete-menu.json";

	private PlayerGameManager gameManager;

	@Override
	public void initializeMenu() {

		File rouletteMenuFile = readFile(APPLICATION_DIRECTORY + ROULETTE_MENU_FILE_JSON);

		Map<String, Map<String, String>> rouletteMenuContent = fileReader.convertFileToMap(rouletteMenuFile);

		currentLangRouletteMenu = rouletteMenuContent.get(Language.getInstance().getLanguage());

		createRoulette();
		// TODO - wrong data & not enough money
		placeABet();
		// TODO - wrong data & not enough money
		printPlayOptions();
	}

	private void createRoulette() {
		gameManager = new PlayerGameManager((Player) UserContainerImpl.getInstance().getLoggedInUser(),
				new Roulette(new ArrayList<Division>()));
	}

	private void placeABet() {
		LOGGER.warn(messages.getYourMoneyMessage() + gameManager.getPlayer().getMoneyAmount());
		LOGGER.warn(messages.getPlaceBetMessage());
		try {
			gameManager.placeBet(new ValidatorUtilImpl().validateDoubleNumber(consoleReader.readLine()));
		} catch (NumberFormatException | IOException e) {
			LOGGER.error(e);
		}
	}

	private File readFile(String path) {
		File file = null;
		try {
			file = fileReader.readFile(path);
		} catch (IOException ioe) {
			LOGGER.error(ioe);
		}

		return file;
	}

	private String userInput(InputReader consInputReader) {
		String input = null;
		try {
			input = consInputReader.readLine();
		} catch (IOException e) {
			LOGGER.error(e);
		}
		return input;
	}

	private void printCurrentMenu(Map<String, String> content) {
		for (String str : content.values()) {
			System.out.println(str);
		}
	}

	private void printPlayOptions() {
		String choice = null;
		boolean quit = false;
		while (!quit) {
			printCurrentMenu(currentLangRouletteMenu);
			choice = userInput(consoleReader);

			switch (choice) {
			case "1":
				quit = playByNumber();
				break;
			case "2":
				quit = playByColor();
				break;
			case "3":
				playBySector();
				break;
			case "4":
				placeABet();
				break;
			case "5":
				quit = true;
				break;
			default:
				LOGGER.warn(messages.getInvalidChoiceMessage());
			}
		}
	}

	private boolean playBySector() {
		try {
			((Roulette) gameManager.getGame())
					.playBySector(new ValidatorUtilImpl().validateIntegerNumber(consoleReader.readLine()));
		} catch (NumberFormatException | IOException e) {
			LOGGER.error(e);
		}
		
		return gameResult();
	}

	private boolean playByColor() {
		try {
			((Roulette) gameManager.getGame()).playByColor(consoleReader.readLine());
		} catch (IOException e) {
			LOGGER.error(e);
		}
		
		return gameResult();
	}

	private boolean playByNumber() {
		try {
			((Roulette) gameManager.getGame())
					.playByNumber(new ValidatorUtilImpl().validateIntegerNumber(consoleReader.readLine()));
		} catch (NumberFormatException | IOException e) {
			LOGGER.error(e);
		}

		return gameResult();
	}

	private boolean gameResult() {
		((Roulette) gameManager.getGame()).spin();
		
		LOGGER.warn(messages.getWinningDevisionMessage() + ((Roulette) gameManager.getGame()).toString());
		
		if(((Roulette) gameManager.getGame()).isWinning()) {
			LOGGER.warn(messages.getYouHaveWonMessage());
		}
		else {
			LOGGER.warn(messages.getYouHaveLostMessage());
		}
		
		return exitConfirmation();
	}
	
	private boolean exitConfirmation() {
		boolean confirmedChoice = false;
		boolean validChoice = false;
		while (!validChoice) {
			System.out.println(messages.getLogOutConfirmationMessage());
			String choice = userInput(consoleReader);
			switch (choice.toLowerCase()) {
			case "y":
				confirmedChoice = true;
				validChoice = true;
				break;
			case "n":
				validChoice = true;
				break;
			default:
				LOGGER.error(messages.getInvalidChoiceMessage());
			}
		}
		return confirmedChoice;
	}
}
