package com.isoft.casino.menu.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.container.impl.UserContainerImpl;
import com.isoft.casino.io.api.FileReader;
import com.isoft.casino.io.api.InputReader;
import com.isoft.casino.io.impl.ConsoleInputReader;
import com.isoft.casino.io.impl.FileReaderImpl;
import com.isoft.casino.language.Language;
import com.isoft.casino.language.Messages;
import com.isoft.casino.menu.api.Menu;
import com.isoft.casino.model.Admin;
import com.isoft.casino.model.Player;
import com.isoft.casino.model.User;

/**
 * Implementation of the menu interface.
 * @see Menu
 * @author Borislav Stoychev
 *
 */
public class AdminMenu implements Menu {
	private static final Logger LOGGER = Logger.getLogger(AdminMenu.class);
	private static final String APPLICATION_DIRECTORY = System.getProperty(GlobalConstants.USER_DIRECTORY_PROPERTY);
	private static final String ADMIN_MENU_FILE_JSON = GlobalConstants.ADMIN_MENU_FILE_PATH;

	private final InputReader consoleReader = new ConsoleInputReader();
	private final FileReader fileReader = new FileReaderImpl();
	
	private Messages messages = new Messages();
	private Map<String,String>currentLangAdminMenu;

	@Override
	public void initializeMenu() 
	{
		File adminMenuFile = readFile(APPLICATION_DIRECTORY + ADMIN_MENU_FILE_JSON);

		Map<String, Map<String, String>>adminMenuContent = fileReader.convertFileToMap(adminMenuFile);
		
		currentLangAdminMenu = adminMenuContent.get(Language.getInstance().getLanguage());
		
		menuOptions();
	}
	
	private File readFile(String path) 
	{
		File file = null;
		try {
			file = fileReader.readFile(path);
		} catch (FileNotFoundException fnfe) {
			LOGGER.error(fnfe);
		} catch (IOException ioe) {
			LOGGER.error(ioe);
		}

		return file;
	}

	private String userInput(InputReader consInputReader)
	{
		String input=null;
		try 
		{
			input = consInputReader.readLine();
		} 
		catch (IOException e) 
		{
			LOGGER.error(e);
		}
		return input;
	}
	
	private void printCurrentMenu(Map<String, String> content) 
	{
		for (String str : content.values()) {
			System.out.println(str);
		}
	}
	
	private boolean logOutConfirmation()
	{
		boolean confirmedChoice = false;
		boolean validChoice = false;
		while(!validChoice)
		{
		    System.out.println(messages.getLogOutConfirmationMessage());
		    String choice = userInput(consoleReader);
		    switch(choice.toLowerCase())
		    {
		    case "y":
			    confirmedChoice = true;
			    validChoice = true;
			    break;
		    case "n":
		    	validChoice = true;
			    break;
		    default:
			    LOGGER.error(messages.getInvalidChoiceMessage());
		    }
		}
		return confirmedChoice;
	}
	
	/**
	 * This method prints all players.
	 */
	private void checkPlayers(Map<String, User> users)
	{
		for (User user : users.values())
		{
			if(user instanceof Player)
			{
			   System.out.println(user);
			}
		}
	}
	
	private void lockPlayer()
	{
		Map<String, User> unlockedUsers = UserContainerImpl.getInstance().getUnlockedUsers();
		
		checkPlayers(unlockedUsers);
		
		String nickNameOfThePlayerToLock = userInput(consoleReader);
		if(nickNameOfThePlayerToLock.equals(GlobalConstants.ADMIN_NICKNAME))
		{
			LOGGER.warn(messages.getCannotLockUserMessages());
		}
		else
		{
		    User playerToLock = UserContainerImpl.getInstance().getUserByNickName(nickNameOfThePlayerToLock);
		    playerToLock.lock();
		}
	}
	
	private void unlockPlayer()
	{
		Map<String, User> lockedUsers = UserContainerImpl.getInstance().getLockedUsers();
		
		checkPlayers(lockedUsers);
		
		String nickNameOfThePlayerToUnlock = userInput(consoleReader);
		if(nickNameOfThePlayerToUnlock.equals(GlobalConstants.ADMIN_NICKNAME))
		{
			LOGGER.warn(messages.getCannotUnlockUserMessages());
		}
		else
		{
		    User playerToUnlock = UserContainerImpl.getInstance().getUserByNickName(nickNameOfThePlayerToUnlock);
		    playerToUnlock.unlock();
		}
	}
	
	private void menuOptions() 
	{
		String choice = null;
		boolean quit = false;
		while(!quit)
		{
			printCurrentMenu(currentLangAdminMenu);
			choice = userInput(consoleReader);
			
			switch(choice)
			{
			case "1":
				Map<String, User> users = UserContainerImpl.getInstance().getUsers();
				checkPlayers(users);
				break;
			case "2":
				lockPlayer();
				break;
			case "3":
				unlockPlayer();
				break;
			case "4":
				break;
			case "5":
				break;
			case "6":
				break;
			case "7":
				Admin loggedInAdmin = (Admin)UserContainerImpl.getInstance().getLoggedInUser();
				if(logOutConfirmation())
				{
					loggedInAdmin.logOut();
					LOGGER.info(loggedInAdmin.getNickname() + GlobalConstants.LOGGED_OUT_MESSAGE);
					quit = true;
				}
				break;
			default:
				LOGGER.warn(messages.getInvalidChoiceMessage());
			}
		}
	}
}
