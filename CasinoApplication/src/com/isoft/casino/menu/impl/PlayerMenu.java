package com.isoft.casino.menu.impl;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.container.impl.UserContainerImpl;
import com.isoft.casino.exception.InvalidAgeException;
import com.isoft.casino.exception.InvalidEmailFormatException;
import com.isoft.casino.exception.InvalidNameFormatException;
import com.isoft.casino.exception.InvalidPassportIdFormatException;
import com.isoft.casino.exception.InvalidPasswordFormatException;
import com.isoft.casino.exception.MismatchingPasswordsException;
import com.isoft.casino.exception.NicknameAlreadyExistsException;
import com.isoft.casino.generator.impl.PasswordHasher;
import com.isoft.casino.io.api.FileReader;
import com.isoft.casino.io.api.InputReader;
import com.isoft.casino.io.api.JsonFileWriter;
import com.isoft.casino.io.impl.ConsoleInputReader;
import com.isoft.casino.io.impl.FileReaderImpl;
import com.isoft.casino.io.impl.JsonFileWriterImpl;
import com.isoft.casino.language.Language;
import com.isoft.casino.language.Messages;
import com.isoft.casino.menu.api.Menu;
import com.isoft.casino.model.Player;
import com.isoft.casino.util.impl.ValidatorUtilImpl;

/**
 * Implementation of the menu interface.
 * 
 * @see Menu
 * @author Borislav Stoychev
 *
 */
public class PlayerMenu implements Menu {
	private static final String APPLICATION_DIRECTORY = System.getProperty(GlobalConstants.USER_DIRECTORY_PROPERTY);
	private static final String PLAYER_MENU_FILE_JSON = GlobalConstants.PLAYER_MENU_FILE_PATH;
	private static final String REGISTER_MENU_FILE_JSON = GlobalConstants.REGISTER_MENU_FILE_PATH;
	private static final String REGISTERED_PLAYERS_FILE_JSON = GlobalConstants.REGISTERED_PLAYERS_FILE_PATH;
	private static final Logger LOGGER = Logger.getLogger(PlayerMenu.class);

	private final PlayerProfileSettingsMenu playerProfileSettingsMenu = new PlayerProfileSettingsMenu();
	private final InputReader consoleReader = new ConsoleInputReader();
	private final FileReader fileReader = new FileReaderImpl();
	private final JsonFileWriter jsonFileWriter = new JsonFileWriterImpl();
	private final ValidatorUtilImpl validator = new ValidatorUtilImpl();

	private Messages messages = new Messages();
	private Map<String, String> currentLangPlayerMenu;

	public void initializeMenu() {
		File playerMenuFile = readFile(APPLICATION_DIRECTORY + PLAYER_MENU_FILE_JSON);

		Map<String, Map<String, String>> playerMenuContent = fileReader.convertFileToMap(playerMenuFile);

		currentLangPlayerMenu = playerMenuContent.get(Language.getInstance().getLanguage());

		menuOptions();
	}

	/**
	 * This method adds the new user in the container.
	 * 
	 * @throws IOException
	 */
	public void register() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		File registeredPlayersFile = readFile(APPLICATION_DIRECTORY + REGISTERED_PLAYERS_FILE_JSON);
		File registerFile = readFile(APPLICATION_DIRECTORY + REGISTER_MENU_FILE_JSON);

		Map<String, Map<String, String>> registerMenuContent = fileReader.convertFileToMap(registerFile);
		Map<String, String> currentLangRegisterMenu = registerMenuContent.get(Language.getInstance().getLanguage());
		Player registeredPlayer = createPlayer(currentLangRegisterMenu, consoleReader);

		LOGGER.info(GlobalConstants.NEW_PLAYER_MESSAGE + registeredPlayer.getNickname() + GlobalConstants.REGISTERED_MESSAGE);
		LOGGER.info(messages.getSuccessfulRegisterMessage());
		UserContainerImpl.getInstance().addUser(registeredPlayer);

		jsonFileWriter.writePlayersToJsonFile(registeredPlayersFile, registeredPlayer, mapper);
	}

	private double moneyParser(String moneyToParse){
		return Double.parseDouble(moneyToParse);
	}

	private void depositMoney(Player loggedInPlayer) {
		double moneyToDeposit = 0d;
		System.out.println(messages.getTypeDepositAmountMessage());
		try {
			moneyToDeposit = moneyParser(userInput(consoleReader));
			if (moneyToDeposit < GlobalConstants.MIN_MONEY_TO_DEPOSIT) {
				LOGGER.warn(messages.getCannotDepositMoneyMessage());
			}
		} catch (NumberFormatException e) {
			LOGGER.error(messages.getInvalidNumberMessage());
		}
		loggedInPlayer.deposit(moneyToDeposit);
	}

	private void withdrawMoney(Player loggedInPlayer) {
		double moneyToWithdraw = 0d;
		System.out.println(messages.getTypeWithdrawAmountMessage());
		try {
			moneyToWithdraw = moneyParser(userInput(consoleReader));
			if (moneyToWithdraw <= GlobalConstants.MIN_MONEY_TO_WITHDRAW || moneyToWithdraw > loggedInPlayer.getMoneyAmount()) {
				LOGGER.warn(messages.getCannotWithdrawMoneyMessage());
			}
		} catch (NumberFormatException e) {
			LOGGER.error(messages.getInvalidNumberMessage());
		}
		loggedInPlayer.withdraw(moneyToWithdraw);
	}

	private void menuOptions() {
		Player loggedInPlayer = (Player) UserContainerImpl.getInstance().getLoggedInUser();
		String choice = null;
		boolean quit = false;
		while (!quit) {
			printCurrentMenu(currentLangPlayerMenu);
			choice = userInput(consoleReader);

			switch (choice) {
			case "1":
				playerProfileSettingsMenu.initializeMenu();
				break;
			case "2":
				depositMoney(loggedInPlayer);
				break;
			case "3":
				withdrawMoney(loggedInPlayer);
				break;
			case "4":
				// TODO: start game A
				break;
			case "5":
				if (logOutConfirmation()) {
					loggedInPlayer.logOut();
					LOGGER.info(loggedInPlayer.getNickname() + GlobalConstants.LOGGED_OUT_MESSAGE);
					quit = true;
				}
				break;
			default:
				LOGGER.warn(messages.getInvalidChoiceMessage());
			}
		}
	}

	private File readFile(String path) {
		File file = null;
		try {
			file = fileReader.readFile(path);
		} catch (IOException e) {
			LOGGER.error(e);
		}
		return file;
	}

	private void printCurrentMenu(Map<String, String> content) {
		for (String str : content.values()) {
			System.out.println(str);
		}
	}

	private boolean logOutConfirmation() {
		boolean confirmedChoice = false;
		boolean validChoice = false;
		while (!validChoice) {
			System.out.println(messages.getLogOutConfirmationMessage());
			String choice = userInput(consoleReader);
			switch (choice.toLowerCase()) {
			case "y":
				confirmedChoice = true;
				validChoice = true;
				break;
			case "n":
				validChoice = true;
				break;
			default:
				LOGGER.error(messages.getInvalidChoiceMessage());
			}
		}
		return confirmedChoice;
	}

	/**
	 * This method creates new player.
	 * 
	 * @param regsiterContent is the content of the register menu
	 * @param consInputReader is the user input reader
	 * @return the created player
	 */
	private Player createPlayer(Map<String, String> regsiterContent, InputReader consInputReader) {
		String firstName = null;
		String middleName = null;
		String lastName = null;
		String nickName = null;
		String ageInput = null;
		int age = 0;
		String email = null;
		String passportId = null;
		String password = null;
		String confirmPassword = null;
		double moneyAmount = 0d;

		for (Map.Entry<String, String> keyValue : regsiterContent.entrySet()) {

			boolean infoIsValid = false;
			while (!infoIsValid) {
				System.out.println(keyValue.getValue());

				try {
					switch (keyValue.getKey()) {
					case "1":
						firstName = userInput(consInputReader);
						validator.validateName(firstName);
						infoIsValid = true;
						break;
					case "2":
						middleName = userInput(consInputReader);
						validator.validateName(middleName);
						infoIsValid = true;
						break;
					case "3":
						lastName = userInput(consInputReader);
						validator.validateName(lastName);
						infoIsValid = true;
						break;
					case "4":
						nickName = userInput(consInputReader);
						validator.validateName(nickName);
						validator.isExistingNicknameOrEmail(nickName);
						infoIsValid = true;
						break;
					case "5":
						ageInput = userInput(consInputReader);
						validator.validateAge(ageInput);
						age = Integer.parseInt(ageInput);
						infoIsValid = true;
						break;
					case "6":
						email = userInput(consInputReader);
						validator.validateEmail(email);
						infoIsValid = true;
						break;
					case "7":
						passportId = userInput(consInputReader);
						validator.validatePassport(passportId);
						infoIsValid = true;
						break;
					case "8":
						password = userInput(consInputReader);
						validator.validatePassword(password);
						infoIsValid = true;
						break;
					case "9":
						confirmPassword = userInput(consInputReader);
						validator.validateConfirmedPassword(confirmPassword, password);
						infoIsValid = true;
						break;
					}
				} catch (InvalidNameFormatException 
						| NicknameAlreadyExistsException
						| NumberFormatException
						| InvalidAgeException
						| InvalidEmailFormatException
						| InvalidPassportIdFormatException
						| InvalidPasswordFormatException
						| MismatchingPasswordsException e) {
					LOGGER.error(e.getMessage());
				}
			}
		}

		password = PasswordHasher.hashPassword(password);

		return new Player(nickName, email, password, firstName, middleName, lastName, age, passportId, moneyAmount);
	}

	private String userInput(InputReader consInputReader) {
		String input = null;
		try {
			input = consInputReader.readLine();
		} catch (IOException e) {
			LOGGER.error(e);
		}
		return input;
	}
}
