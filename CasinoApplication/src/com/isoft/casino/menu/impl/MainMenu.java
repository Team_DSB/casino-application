package com.isoft.casino.menu.impl;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.container.impl.UserContainerImpl;
import com.isoft.casino.exception.LockedPlayerException;
import com.isoft.casino.exception.NotExistingNicknameException;
import com.isoft.casino.exception.WrongPasswordException;
import com.isoft.casino.generator.impl.PasswordHasher;
import com.isoft.casino.io.api.FileReader;
import com.isoft.casino.io.api.InputReader;
import com.isoft.casino.io.impl.ConsoleInputReader;
import com.isoft.casino.io.impl.FileReaderImpl;
import com.isoft.casino.language.Language;
import com.isoft.casino.language.Messages;
import com.isoft.casino.menu.api.Menu;
import com.isoft.casino.menu.api.UserMenu;
import com.isoft.casino.model.Admin;
import com.isoft.casino.model.User;
import com.isoft.casino.util.impl.ValidatorUtilImpl;

/**
 * The main menu manager for the application.
 * @see Menu
 * @see UserMenu
 * @author Borislav Stoychev
 *
 */
public class MainMenu implements Menu, UserMenu{
	private static final String APPLICATION_DIRECTORY = System.getProperty(GlobalConstants.USER_DIRECTORY_PROPERTY);
	private static final String MAIN_MENU_FILE_JSON = GlobalConstants.MAIN_MENU_FILE_PATH;
	private static final String LOGIN_MENU_FILE_JSON = GlobalConstants.LOGIN_MENU_FILE_PATH;
	private static final String REGISTERED_PLAYERS_FILE_JSON = GlobalConstants.REGISTERED_PLAYERS_FILE_PATH;
	private static final Logger LOGGER = Logger.getLogger(MainMenu.class);
	
	private final AdminMenu adminMenu = new AdminMenu();
	private final PlayerMenu playerMenu = new PlayerMenu();
	
	private final InputReader consoleReader = new ConsoleInputReader();
	private final FileReader fileReader = new FileReaderImpl();
	private final ValidatorUtilImpl validator = new ValidatorUtilImpl();
	
	private Messages messages = new Messages();
	private Map<String,String>currentLangMainMenu;
	
	@Override
	public void initializeMenu()
	{
     	File mainMenuFile = readFile(APPLICATION_DIRECTORY + MAIN_MENU_FILE_JSON);
     	
     	Map<String, Map<String, String>>mainMenuContent = fileReader.convertFileToMap(mainMenuFile);
		
     	currentLangMainMenu = mainMenuContent.get(Language.getInstance().getLanguage());
     	
     	File registeredUsers = readFile(APPLICATION_DIRECTORY + REGISTERED_PLAYERS_FILE_JSON);
     	
     	UserContainerImpl.getInstance().updateContainer(registeredUsers);
				
		menuOptions();
	}
	
	private File readFile(String path) 
	{
		File file = null;
		try 
		{
			file = fileReader.readFile(path);
		} 
		catch (IOException ioe) 
		{
			LOGGER.error(ioe);
		}

		return file;
	}

	private void printCurrentMenu(Map<String, String> content) 
	{
		for (String str : content.values()) 
		{
			System.out.println(str);
		}
	}
	
	private String userInput(InputReader consInputReader)
	{
		String input=null;
		try 
		{
			input = consInputReader.readLine();
		} 
		catch (IOException e) 
		{
			LOGGER.error(e);
		}
		return input;
	}
	
	
	@Override
	public User login()
	{
		File loginFile = readFile(APPLICATION_DIRECTORY + LOGIN_MENU_FILE_JSON);
		Map<String, Map<String, String>> loginMenuContent = fileReader.convertFileToMap(loginFile);
		Map<String, String> currentLangLoginMenu = loginMenuContent.get(Language.getInstance().getLanguage());
		String nickNameOrEmail = null;
		
		User loggedInUser = null;

		for (Map.Entry<String, String> keyValue : currentLangLoginMenu.entrySet()) 
		{
			boolean infoIsValid = false;
			while (!infoIsValid) 
			{
				System.out.println(keyValue.getValue());
				try
				{
				   switch(keyValue.getKey()) 
				   {
				      case "1":
				    	nickNameOrEmail = userInput(consoleReader);
					   validator.isNotExistingNicknameOrEmail(nickNameOrEmail);
					   infoIsValid = true;
					   break;
				      case "2":
					   validatePassword(nickNameOrEmail);
					   if(nickNameOrEmail.contains(GlobalConstants.EMAIL_IDENTIFIER)) {
						   loggedInUser = UserContainerImpl.getInstance().getUserByEmail(nickNameOrEmail);
					   } else {
						   loggedInUser = UserContainerImpl.getInstance().getUserByNickName(nickNameOrEmail);
					   }
					   infoIsValid = true;
					   break;
				   }
				}
				catch (NotExistingNicknameException e)
				{
					LOGGER.error(e.getMessage());
				}
				catch (LockedPlayerException e)
				{
					LOGGER.error(e.getMessage());
					infoIsValid = true;
				}
			}
		}
		
		return loggedInUser;
	}
	
	/**
	 * 
	 * @param nickname of the user that wants to log in
	 * @return whether the user has logged in successfully
	 */
	private void validatePassword(String nickNameOrEmail) throws LockedPlayerException
	{
		int passwordAttempts = 3;
		User currentUser = null;
		if(nickNameOrEmail.contains(GlobalConstants.EMAIL_IDENTIFIER)){
			currentUser = UserContainerImpl.getInstance().getUserByEmail(nickNameOrEmail);
		} else {
			currentUser = UserContainerImpl.getInstance().getUserByNickName(nickNameOrEmail);
		}
		
		if(currentUser.isLocked()) {
			throw new LockedPlayerException(messages.getLockedPlayerExceptionMessage());
		} else {
			while(passwordAttempts!=0)
			{
				String password = userInput(consoleReader);
				password = PasswordHasher.hashPassword(password);
				try
				{
				if(currentUser.getPassword().equals(password))
				{
					 break;
				}
				else
				{
					 passwordAttempts--;
					 throw new WrongPasswordException(messages.getWrongPasswordExceptionMessage()+" "+messages.getAttemptsLeftMessage()+passwordAttempts);
				}
				}
				catch (WrongPasswordException e)
				{
					LOGGER.error(e.getMessage());
				}
		    }
		
		
		if(passwordAttempts==0)
		{
			currentUser.lock();
			throw new LockedPlayerException(messages.getLockedPlayerExceptionMessage());
		}
	}
			
	}
	
	private void menuOptions()
	{
		String choice = null;
		boolean quit = false;
		User loggedInUser = null;
		while(!quit)
		{
			printCurrentMenu(currentLangMainMenu);
			choice = userInput(consoleReader);
			
			try{
				switch(choice) {
				case "1":
					loggedInUser = login();
					if(loggedInUser == null)
					{
						break;
					}
					else if(loggedInUser instanceof Admin)
					{
						loggedInUser.logIn();
						LOGGER.info(loggedInUser.getNickname()+ GlobalConstants.LOGGED_IN_MESSAGE);
						LOGGER.info(messages.getSuccessfulLoginMessage());
						adminMenu.initializeMenu();
					}
					else
					{
						loggedInUser.logIn();
						LOGGER.info(loggedInUser.getNickname()+ GlobalConstants.LOGGED_IN_MESSAGE);
						LOGGER.info(messages.getSuccessfulLoginMessage());
						playerMenu.initializeMenu();
					}
					break;
				case "2":
					playerMenu.register();
					break;
				case "3": 
					quit = true;
				    break;
				default:
					LOGGER.warn(messages.getInvalidChoiceMessage());
				}
			}catch (IOException ex) {
				LOGGER.error(ex.getMessage());
			}
			
		}
	}
}
