package com.isoft.casino.menu.impl;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.isoft.casino.constants.GlobalConstants;
import com.isoft.casino.io.api.FileReader;
import com.isoft.casino.io.api.InputReader;
import com.isoft.casino.io.impl.ConsoleInputReader;
import com.isoft.casino.io.impl.FileReaderImpl;
import com.isoft.casino.language.Language;
import com.isoft.casino.menu.api.Menu;

/**
 * Implementation of menu interface.
 * @see Menu
 * @author Borislav Stoychev
 *
 */
public class LanguageMenu implements Menu{
	private static final Logger LOGGER = Logger.getLogger(LanguageMenu.class);
	
	private static final String APPLICATION_DIRECTORY = System.getProperty(GlobalConstants.USER_DIRECTORY_PROPERTY);
	private static final String LANGUAGE_MENU_FILE_JSON = GlobalConstants.LANGUAGE_MENU_FILE_PATH;

	private final InputReader consoleReader = new ConsoleInputReader();
	private final FileReader fileReader = new FileReaderImpl();
	
	@Override
	public void initializeMenu()
	{
		File languageFile = readFile(APPLICATION_DIRECTORY + LANGUAGE_MENU_FILE_JSON);
		Map<String, Map<String, String>> languageMenuContent = fileReader.convertFileToMap(languageFile);
		
		boolean quit = false;
		while(!quit)
		{
			printCurrentMenu(languageMenuContent);
			String choice = userInput(consoleReader);
			switch(choice)
			{
			case "1":
				Language.getInstance().setLanguage(GlobalConstants.ENGLISH_LANGUAGE);
				quit = true;
				break;
			case "2":
				Language.getInstance().setLanguage(GlobalConstants.BULGARIAN_LANGUAGE);
				quit = true;
				break;
			default:
				LOGGER.warn(GlobalConstants.INVALID_CHOICE_MESSAGE);
			}
		}
	}
	
	private String userInput(InputReader consInputReader)
	{
		String input=null;
		try 
		{
			input = consInputReader.readLine();
		} 
		catch (IOException e) 
		{
			LOGGER.error(e);
		}
		return input;
	}
	
	private void printCurrentMenu(Map<String, Map<String,String>> content) 
	{
		for (Map<String,String> str : content.values()) {
			str.values().forEach(System.out::println);
		}
	}
	
	private File readFile(String path) 
	{
		File file = null;
		try {
			file = fileReader.readFile(path);
		} catch (IOException ioe) {
			LOGGER.error(ioe);
		}

		return file;
	}

	
}
