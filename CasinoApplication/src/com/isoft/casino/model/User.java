package com.isoft.casino.model;

/**
 * The User class holds information about the user: nickname, email, password,
 * is the user lock or logged in.
 * 
 * @author Sonya Lazarova
 *
 */

public abstract class User {

	private String nickname;
	private String email;
	private String password;
	private boolean isLoggedIn;
	private boolean isLocked;

	/**
	 * 
	 * @param nickname
	 * @param email
	 * @param password
	 */
	protected User(String nickname, String email, String password) {
		this.nickname = nickname;
		this.email = email;
		this.password = password;
		this.isLoggedIn = false;
		this.isLocked = false;
	}

	/**
	 * Change the log in flag to true
	 */
	public void logIn() {
		isLoggedIn = true;
	}

	/**
	 * Change the log in flag to false
	 */
	public void logOut() {
		isLoggedIn = false;
	}

	/**
	 * Change the lock flag to true
	 */
	public void lock() {
		isLocked = true;
	}

	/**
	 * Change the unlock flag to false
	 */
	public void unlock() {
		isLocked = false;
	}

	public boolean isLocked() {
		return isLocked;
	}

	public String getNickname() {
		return nickname;
	}

	public String getPassword() {
		return password;
	}

	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	public String getEmail() {
		return email;
	}

	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param isLoggedIn the isLoggedIn to set
	 */
	public void setLoggedIn(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}

	/**
	 * @param isLocked the isLocked to set
	 */
	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}
}
