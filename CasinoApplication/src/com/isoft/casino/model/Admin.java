package com.isoft.casino.model;

import com.isoft.casino.generator.impl.PasswordHasher;

/**
 * The Admin class holds singleton implementation of the admin user.
 * 
 * @author Sonya Lazarova
 *
 * @see User
 */

public class Admin extends User {
	private static final Admin instance = new Admin("admin", "admin@admin.com", PasswordHasher.hashPassword("admin"));

	private Admin(String nickname, String email, String password) {
		super(nickname, email, password);
	}

	/**
	 * 
	 * @return the Admin user
	 * 
	 */
	public static Admin getInstance() {
		return instance;
	}
}
