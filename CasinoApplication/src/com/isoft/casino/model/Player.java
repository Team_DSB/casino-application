package com.isoft.casino.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Player class holds information about the player user: name, age, passport
 * number, money amount
 * 
 * @author Sonya Lazarova
 *
 * @see User
 */

public class Player extends User {

	private String firstName;
	private String middleName;
	private String lastName;
	private int age;
	private String passportId;
	private double moneyAmount;

	/**
	 * @param nickname
	 * @param email
	 * @param password
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @param age
	 * @param passportId
	 * @param moneyAmount
	 */
	@JsonCreator
	public Player(@JsonProperty("nick_name") String nickname, @JsonProperty("email") String email,
			@JsonProperty("password") String password, @JsonProperty("first_name") String firstName,
			@JsonProperty("middle_name") String middleName, @JsonProperty("last_name") String lastName,
			@JsonProperty("age") int age, @JsonProperty("passport_id") String passportId,
			@JsonProperty("money") double moneyAmount) {
		super(nickname, email, password);
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.age = age;
		this.passportId = passportId;
		this.moneyAmount = moneyAmount;
	}

	/**
	 * Adding the deposit money to the money amount
	 * 
	 * @param moneyToDeposit - the deposit money which are going to be add to the
	 *                       money amount
	 */
	public void deposit(double moneyToDeposit) {
		moneyAmount += moneyToDeposit;
	}

	/**
	 * Withdrawing money from the money amount
	 * 
	 * @param moneyToDeposit - the money which are going to be withdraw from the
	 *                       money amount
	 */
	public void withdraw(double moneyToWithdraw) {
		if (moneyAmount >= moneyToWithdraw) {

			moneyAmount -= moneyToWithdraw;
		}
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @return the passportId
	 */
	public String getPassportId() {
		return passportId;
	}

	/**
	 * @return the moneyAmount
	 */
	public double getMoneyAmount() {
		return moneyAmount;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @param passportId the passportId to set
	 */
	public void setPassportId(String passportId) {
		this.passportId = passportId;
	}

	/**
	 * @param moneyAmount the moneyAmount to set
	 */
	public void setMoneyAmount(double moneyAmount) {
		this.moneyAmount = moneyAmount;
	}

	@Override
	public String toString() {
		return "Player: [nickname=" + getNickname() + ", email=" + getEmail() + ", firstName=" + firstName
				+ ", middleName=" + middleName + ", lastName=" + lastName + ", age=" + age + ", passportId="
				+ passportId + ", moneyAmount=" + moneyAmount + "," + "isLocked=" + isLocked() + ", isLoggedIn="
				+ isLoggedIn() + "]";
	}

}
