package com.isoft.casino.constants;

/**
 * Class containing all string values defined as constants: file paths,
 * messages, variable keys etc.
 * 
 * @author Deyan Georgiev
 */
public final class GlobalConstants {

	/**
	 * private constructor to prevent instantiation
	 */
	private GlobalConstants() {
	}

	public static final String EMAIL_IDENTIFIER = "@";

	public static final int ROULLETTE_ZERO_COEFFICIENT = 0;
	public static final int ROULLETTE_TWO_COEFFICIENT = 2;
	public static final int ROULLETTE_THIRTY_SIX_COEFFICIENT = 36;
	public static final int ROULLETTE_THREE_COEFFICIENT = 3;
	public static final int BLACKJACK_MAX_VALUE = 21;
	
	public static final int CARD_DEFAULT_VALUE = 0;
	public static final int CARD_TWO_VALUE = 2;
	public static final int CARD_THREE_VALUE = 3;
	public static final int CARD_FOUR_VALUE = 4;
	public static final int CARD_FIVE_VALUE = 5;
	public static final int CARD_SIX_VALUE = 6;
	public static final int CARD_SEVEN_VALUE = 7;
	public static final int CARD_EIGHT_VALUE = 8;
	public static final int CARD_NINE_VALUE = 9;
	public static final int CARD_TEN_VALUE = 10;
	
	public static final String MD5_HASH_NAME = "MD5";

	public static final int DIVISION_START_NUMBER_GENERATOR = 1;
	public static final int DIVISION_END_NUMBER_GENERATOR = 38;

	public static final String USER_DIRECTORY_PROPERTY = "user.dir";

	public static final String MESSAGES_FILE_PATH = "/src/com/isoft/casino/resource/file/messages.json";
	public static final String ADMIN_MENU_FILE_PATH = "/src/com/isoft/casino/resource/file/admin-menu.json";
	public static final String MAIN_MENU_FILE_PATH = "/src/com/isoft/casino/resource/file/main-menu.json";
	public static final String LOGIN_MENU_FILE_PATH = "/src/com/isoft/casino/resource/file/login-menu.json";
	public static final String REGISTERED_PLAYERS_FILE_PATH = "/src/com/isoft/casino/resource/file/registered-players.json";
	public static final String PLAYER_MENU_FILE_PATH = "/src/com/isoft/casino/resource/file/player-menu.json";
	public static final String REGISTER_MENU_FILE_PATH = "/src/com/isoft/casino/resource/file/register-menu.json";
	public static final String PLAYER_PROFILE_SETTINGS_MENU_FILE_PATH = "/src/com/isoft/casino/resource/file/player-profile-settings.json";
	public static final String LANGUAGE_MENU_FILE_PATH = "/src/com/isoft/casino/resource/file/language-menu.json";
	public static final String ROULETTE_MENU_FILE_PATH = "/src/com/isoft/casino/resource/file/roulete-menu.json";

	public static final String ADMIN_NICKNAME = "admin";
	public static final String LOGGED_OUT_MESSAGE = " has just logged out.";
	public static final String LOGGED_IN_MESSAGE = " has just logged in.";
	public static final String REGISTERED_MESSAGE = " has just been registered.";
	public static final String NEW_PLAYER_MESSAGE = "A new player ";
	public static final String ENGLISH_LANGUAGE = "en";
	public static final String BULGARIAN_LANGUAGE = "bg";
	public static final String INVALID_CHOICE_MESSAGE = "Invalid choice";

	public static final int MIN_MONEY_TO_DEPOSIT = 5;
	public static final int MIN_MONEY_TO_WITHDRAW = 0;

	public static final String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
	public static final String PASSPORT_REGEX = "^\\d{9}$";

}
