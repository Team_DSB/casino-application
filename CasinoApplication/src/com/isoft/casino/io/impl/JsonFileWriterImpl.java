package com.isoft.casino.io.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SequenceWriter;
import com.isoft.casino.container.impl.UserContainerImpl;
import com.isoft.casino.io.api.JsonFileWriter;
import com.isoft.casino.model.Player;

/**
 * Implementation of the FileWriter class
 * 
 * @author Deyan Georgiev
 * @see JsonFileWriter
 */
public class JsonFileWriterImpl implements JsonFileWriter {

	@Override
	public void writePlayersToJsonFile(File file, Player registeredPlayer, ObjectMapper mapper) throws IOException {
		if (file.length() == 0) {
			FileWriter writer = new FileWriter(file);
			SequenceWriter seqWriter = mapper.writer().writeValuesAsArray(writer);
			seqWriter.write(registeredPlayer);
			seqWriter.close();
		} else {
			List<Player> allRegisteredPlayers = new ArrayList<>(Arrays.asList(mapper.readValue(file, Player[].class)));
			Player modifiedPlayer = modifiyProfileChanges(registeredPlayer, allRegisteredPlayers);
			if (modifiedPlayer != null) {
				allRegisteredPlayers.removeIf(pl -> (pl.getNickname().equals(registeredPlayer.getNickname())
						|| pl.getEmail().equals(registeredPlayer.getEmail())));
				allRegisteredPlayers.add(modifiedPlayer);
			} else {
				allRegisteredPlayers.add(registeredPlayer);
			}

			mapper.writeValue(file, allRegisteredPlayers);
			UserContainerImpl.getInstance().updateContainer(file);
		}
	}

	private Player modifiyProfileChanges(Player player, List<Player> allPlayers) {
		Optional<Player> optionalPlayer = allPlayers.stream()
				.filter(pl -> pl.getNickname().equals(player.getNickname())
						|| pl.getPassportId().equals(player.getPassportId()) || pl.getEmail().equals(player.getEmail()))
				.findFirst();

		if (optionalPlayer.isPresent()) {
			Player currentPlayer = optionalPlayer.get();
			currentPlayer.setAge(player.getAge());
			currentPlayer.setEmail(player.getEmail());
			currentPlayer.setFirstName(player.getFirstName());
			currentPlayer.setMiddleName(player.getMiddleName());
			currentPlayer.setLastName(player.getLastName());
			currentPlayer.setNickname(player.getNickname());
			currentPlayer.setPassportId(player.getPassportId());
			currentPlayer.setPassword(player.getPassword());
			return currentPlayer;
		}

		return null;
	}

}
