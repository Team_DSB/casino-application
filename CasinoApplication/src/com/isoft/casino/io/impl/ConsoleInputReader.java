package com.isoft.casino.io.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.isoft.casino.io.api.InputReader;

/**
 * ConsoleInputReader class implements InputReader in order to read a line from
 * the console.
 * 
 * @author Borislav Stoychev
 * @see InputReader
 *
 */
public class ConsoleInputReader implements InputReader {

	private BufferedReader reader;

	public ConsoleInputReader() {
		this.reader = new BufferedReader(new InputStreamReader(System.in));
	}

	@Override
	public String readLine() throws IOException {
		return reader.readLine();
	}
}
