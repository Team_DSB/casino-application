package com.isoft.casino.io.impl;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.casino.io.api.FileReader;

/**
 * @author Borislav Stoychev
 * @see FileReader
 */
public class FileReaderImpl implements FileReader {

	private static final Logger LOGGER = Logger.getLogger(FileReaderImpl.class);

	@Override
	public File readFile(String filePath) {
		return new File(filePath);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Map<String, String>> convertFileToMap(File file) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(file, Map.class);
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
		}
		return null;
	}
}
