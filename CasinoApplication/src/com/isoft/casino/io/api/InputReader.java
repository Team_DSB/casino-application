package com.isoft.casino.io.api;

import java.io.IOException;

/**
 * The interface InputReader main task is to read a line from the console as a
 * user input.
 * 
 * @author Borislav Stoychev
 *
 */
public interface InputReader {

	/**
	 * @return String representation of the user input.
	 * @throws IOException
	 */
	public String readLine() throws IOException;
}
