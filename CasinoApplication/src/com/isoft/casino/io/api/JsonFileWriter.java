package com.isoft.casino.io.api;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.casino.model.Player;

/**
 * Interface class defining the method for saving the registered players into a
 * file
 * 
 * @author Deyan Georgiev
 */
public interface JsonFileWriter {

	/**
	 * Method for saving a registered player in a file
	 * 
	 * @param file   the file in which to be saved
	 * @param player the player object
	 * @param mapper the mapper to json
	 * @throws IOException if the specified file does not exist
	 */
	public void writePlayersToJsonFile(File file, Player player, ObjectMapper mapper) throws IOException;
}
