package com.isoft.casino.io.api;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * The interface FileReader contains 2 methods, which are based on reading a
 * current file with specified file path and converting the file into a map with
 * keys and values.
 * 
 * @author Borislav Stoychev
 *
 */
public interface FileReader {

	/**
	 * @param filePath the path for the searched file
	 * @return the found file
	 * @throws IOException in case there is no file at the specified path
	 */
	public File readFile(String filePath) throws IOException;

	/**
	 * @param file the read file
	 * @return map with keys and values from the read file
	 */
	public Map<String, Map<String, String>> convertFileToMap(File file);
}
